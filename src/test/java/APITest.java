package test.java;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class APITest extends BaseTest {

    @BeforeClass
    public void beforeClass() {
        Util.log("beforeClass");
        Util.log("loading testing data into environment.");
    }

    @AfterClass
    public void afterClass() {
        Util.log("afterClass");
    }
}
