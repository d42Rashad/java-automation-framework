package test.java;

import org.testng.annotations.*;

public abstract class BaseTest {

    @BeforeSuite
    public void beforeSuite(){
        Util.log("beforeSuite");
        Util.log("initializing environment.");
    }

    @AfterSuite
    public void afterSuite(){
        Util.log("afterSuite");
        Util.log("destroying environment.");
        Util.log("archiving test results.");
    }

    @BeforeMethod
    public void beforeMethod() {
        Util.log("beforeMethod");
        Util.log("loading test data into memory");
    }

    @AfterMethod
    public void afterMethod() {
        Util.log("afterMethod");
    }

    @BeforeTest
    public void beforeTest() {
        Util.log("beforeTest");
    }

    @AfterTest
    public void afterTest() {
        Util.log("afterTest");
    }
}
