package test.java;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FreeTrialPage {

    private WebDriver driver;

    By spanSelfHostedVersion;
    public WebElement getSpanSelfHostedVersion() {
        return  driver.findElement(spanSelfHostedVersion);
    }

    By spanSelfCloudVersion;
    public WebElement getSpanSelfCloudVersion() {
        return  driver.findElement(spanSelfCloudVersion);
    }

    By inputFirstName;
    public WebElement getInputFirstName() {
        return  driver.findElement(inputFirstName);
    }

    By inputLastName;
    public WebElement getInputLastName() {
        return  driver.findElement(inputLastName);
    }

    By inputCompany;
    public WebElement getInputCompany() {
        return  driver.findElement(inputCompany);
    }

    By inputEmail;
    public WebElement getInputEmail() {
        return  driver.findElement(inputEmail);
    }

    By inputPhone;
    public WebElement getInputPhone() {
        return  driver.findElement(inputPhone);
    }

    By selectNumDevices;
    public WebElement getSelectNumDevices() {
        return  driver.findElement(selectNumDevices);
    }

    By inputPhysicalVirtualInfrastructure;
    public WebElement getInputPhysicalVirtualInfrastructure() {
        return  driver.findElement(inputPhysicalVirtualInfrastructure);
    }

    By inputOSorSoftware;
    public WebElement getInputOSorSoftware() {
        return  driver.findElement(inputOSorSoftware);
    }

    By inputApplicationDiscoveryAndDependencyMapping;
    public WebElement getInputApplicationDiscoveryAndDependencyMapping() {
        return  driver.findElement(inputApplicationDiscoveryAndDependencyMapping);
    }

    By inputCloudAndDataCenterMigration;
    public WebElement getInputCloudAndDataCenterMigration() {
        return  driver.findElement(inputCloudAndDataCenterMigration);
    }

    By inputITSMIntegrationAndAutomation;
    public WebElement getInputITSMIntegrationAndAutomation() {
        return  driver.findElement(inputITSMIntegrationAndAutomation);
    }

    By inputHardwareOrSoftwareAudit;
    public WebElement getInputHardwareOrSoftwareAudit() {
        return  driver.findElement(inputHardwareOrSoftwareAudit);
    }

    By inputCloudAndDataCenterManagement;
    public WebElement getInputCloudAndDataCenterManagement() {
        return  driver.findElement(inputCloudAndDataCenterManagement);
    }

    By inputYes;
    public WebElement getInputYes() {
        return  driver.findElement(inputYes);
    }

    By inputNo;
    public WebElement getInputNo() {
        return  driver.findElement(inputNo);
    }

    By inputTermsAndConditions;
    public WebElement getInputTermsAndConditions() {
        return  driver.findElement(inputTermsAndConditions);
    }

    By inputTrialButton;
    public WebElement getInputTrialButton() {
        return  driver.findElement(inputTrialButton);
    }

    By inputCloudPassword;
    public WebElement getInputCloudPassword() {
        return  driver.findElement(inputCloudPassword);
    }


    public FreeTrialPage(WebDriver driver, String url) throws IllegalAccessException {
        this.driver = driver;
        ObjectRepositoryParser.fillInClass(this,"FreeTrialPage");
        driver.get(url);
    }

    public void checkPageElements() {
        Assert.assertTrue(getSpanSelfHostedVersion().isDisplayed());
        Assert.assertTrue(getSpanSelfCloudVersion().isDisplayed());
        Assert.assertTrue(getInputFirstName().isDisplayed());
        Assert.assertTrue(getInputLastName().isDisplayed());
        Assert.assertTrue(getInputCompany().isDisplayed());
        Assert.assertTrue(getInputEmail().isDisplayed());

        Assert.assertTrue(getInputPhone().isDisplayed());
        Assert.assertTrue(getSelectNumDevices().isDisplayed());
        Assert.assertTrue(getInputPhysicalVirtualInfrastructure().isDisplayed());
        Assert.assertTrue(getInputOSorSoftware().isDisplayed());
        Assert.assertTrue(getInputApplicationDiscoveryAndDependencyMapping().isDisplayed());
        Assert.assertTrue(getInputCloudAndDataCenterMigration().isDisplayed());
        Assert.assertTrue(getInputITSMIntegrationAndAutomation().isDisplayed());
        Assert.assertTrue(getInputHardwareOrSoftwareAudit().isDisplayed());
        Assert.assertTrue(getInputCloudAndDataCenterManagement().isDisplayed());
        Assert.assertTrue(getInputYes().isDisplayed());

        Assert.assertTrue(getInputNo().isDisplayed());
        Assert.assertTrue(getInputTermsAndConditions().isDisplayed());
        Assert.assertTrue(getInputTrialButton().isDisplayed());
    }
}
