package test.java;

import org.testng.annotations.Test;
import test.java.utility.UITest;

public class FreeTrialTest extends UITest {

    @Test
    public void downloadSelfHostedVersionFreeTrial() throws IllegalAccessException {
        FreeTrialPage ftp = new FreeTrialPage(driver,"https://www.device42.com/download/");
        ftp.checkPageElements();
        click(ftp.getSpanSelfHostedVersion());
        sendKeys(ftp.getInputFirstName(),"Rashad");
        sendKeys(ftp.getInputLastName(),"Colebrooke");
        sendKeys(ftp.getInputCompany(),"Device42");
        sendKeys(ftp.getInputEmail(),"rashad.colebrooke@device42.com");
        sendKeys(ftp.getInputPhone(),"860 - 710 - 3210");
        selectByValue(ftp.getSelectNumDevices(), "I don't know");
        click(ftp.getInputPhysicalVirtualInfrastructure());
        click(ftp.getInputOSorSoftware());
        click(ftp.getInputApplicationDiscoveryAndDependencyMapping());
        click(ftp.getInputCloudAndDataCenterMigration());
        click(ftp.getInputITSMIntegrationAndAutomation());
        click(ftp.getInputHardwareOrSoftwareAudit());
        click(ftp.getInputCloudAndDataCenterManagement());
        click(ftp.getInputYes());
        click(ftp.getInputTermsAndConditions());
        click(ftp.getInputTermsAndConditions());
    }

    @Test
    public void downloadCloudVersionFreeTrial() throws IllegalAccessException {
        FreeTrialPage ftp = new FreeTrialPage(driver,"https://www.device42.com/download/");
        ftp.checkPageElements();
        click(ftp.getSpanSelfCloudVersion());
        sendKeys(ftp.getInputCloudPassword(),"Password");
        sendKeys(ftp.getInputFirstName(),"Rashad");
        sendKeys(ftp.getInputLastName(),"Colebrooke");
        sendKeys(ftp.getInputCompany(),"Device42");
        sendKeys(ftp.getInputEmail(),"rashad.colebrooke@device42.com");
        sendKeys(ftp.getInputPhone(),"860 - 710 - 3210");
        selectByValue(ftp.getSelectNumDevices(), "I don't know");
        click(ftp.getInputPhysicalVirtualInfrastructure());
        click(ftp.getInputOSorSoftware());
        click(ftp.getInputApplicationDiscoveryAndDependencyMapping());
        click(ftp.getInputCloudAndDataCenterMigration());
        click(ftp.getInputITSMIntegrationAndAutomation());
        click(ftp.getInputHardwareOrSoftwareAudit());
        click(ftp.getInputCloudAndDataCenterManagement());
        click(ftp.getInputYes());
        click(ftp.getInputTermsAndConditions());
        click(ftp.getInputTermsAndConditions());
    }

}
