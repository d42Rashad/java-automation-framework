package test.java;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class Listener implements ITestListener {

    @Override
    public void onTestStart(ITestResult iTestResult) {
        //Util.log("onTestStart");
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        //Util.log("onTestSuccess");
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        Util.log("!!!!!!!!!!!onTestFailure!!!!!!!!!!!");
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        //Util.log("onTestSkipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        //Util.log("onTestFailedButWithinSuccessPercentage");
    }

    @Override
    public void onStart(ITestContext iTestContext) {
        //Util.log("onStart");
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        //Util.log("onFinish");
    }
}
