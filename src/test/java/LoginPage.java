package test.java;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

    private WebDriver driver;

    By imgCompanyLogo;
    By inputUsername;
    By inputPassword;
    By selectLanguage;
    By buttonLogin;
    By aApplicationManager;

    public LoginPage(WebDriver driver, String url) throws IllegalAccessException {
        this.driver = driver;
        ObjectRepositoryParser.fillInClass(this,"LoginPage");
        driver.get(url);
    }

    public void checkPageElements() {
        Assert.assertTrue(driver.findElement(imgCompanyLogo).isDisplayed());
        Assert.assertTrue(driver.findElement(inputUsername).isDisplayed());
        Assert.assertTrue(driver.findElement(inputPassword).isDisplayed());
        Assert.assertTrue(driver.findElement(selectLanguage).isDisplayed());
        Assert.assertTrue(driver.findElement(buttonLogin).isDisplayed());
        Assert.assertTrue(driver.findElement(aApplicationManager).isDisplayed());
    }

    public void login(String username, String password) {
        driver.findElement(inputUsername).sendKeys(username);
        driver.findElement(inputPassword).sendKeys(password);
        driver.findElement(buttonLogin).click();
    }

}
