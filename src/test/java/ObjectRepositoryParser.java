package test.java;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;

public class ObjectRepositoryParser {

    public static void fillInClass(Object obj, String objectRepo) throws IllegalAccessException {
        JSONObject objectRepository = getObjectRepository(objectRepo);
        for (Field f: obj.getClass().getDeclaredFields()) {
            if (f.getType() == By.class) {
                JSONObject object = (JSONObject)objectRepository.get(f.getName());
                switch(object.get("type").toString()) {
                    case "xpath":
                        f.set(obj,By.xpath(object.get("val").toString()));
                        break;
                }
            }
        }
    }

    private static JSONObject getObjectRepository(String page) {
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader("src" + File.separator + "test" + File.separator + "object-repository" + File.separator +page + ".json")) {
            Object obj = jsonParser.parse(reader);
            return (JSONObject) obj;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

}