package test.java;

import org.testng.annotations.Test;
import test.java.utility.UITest;

public class TestClass2 extends UITest {

    @Test
    public void testMethodOne() throws InterruptedException {
        driver.get("https://www.google.com/");
        Thread.sleep(3000);
    }

    @Test
    public void testMethodTwo() throws IllegalAccessException {
        LoginPage hp = new LoginPage(driver,"https://10.90.4.3/");
        hp.checkPageElements();
        hp.login("admin", "adm!nd42");
    }

}
