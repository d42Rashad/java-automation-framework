package test.java;

import org.testng.Reporter;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Util {

    public static boolean pingHost(String host, int port, int timeout) {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(host, port), timeout);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static void log(String msg) {
        Reporter.log("[ThreadID: " + Thread.currentThread().getId() + "]" + msg, true);
    }

}
