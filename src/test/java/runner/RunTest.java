package test.java.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.Test;

@CucumberOptions(features={"src//test//java//features"}
        ,glue={"classpath:test.java.stepdefinitions","classpath:test.java.utility"}
        ,plugin = {"pretty", "html:target/cucumber"}
        , tags ={"@web"})

@Test
public class RunTest extends AbstractTestNGCucumberTests {

}