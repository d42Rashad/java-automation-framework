package test.java.utility;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import test.java.BaseTest;
import test.java.Util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

public class UITest extends BaseTest {

    enum Browser { CHROME, FIREFOX }

    public static WebDriver driver;


    @BeforeClass
    @Before("@web")
    public void beforeClass() throws MalformedURLException {

        String platform = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("platform");
        String browser = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("browser");
        String browser_version = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("browser_version");

        Util.log("beforeClass");
        Util.log("loading testing data into environment.");

        Util.log("Platform: " + platform);
        Util.log("Browser: " + browser);
        Util.log("Browser Version: " + browser_version);

        String grid_url = checkGrid();
        switch(Enum.valueOf(Browser.class, browser.trim())){
            case CHROME:
                ChromeOptions chrome_options = new ChromeOptions();
                chrome_options.setCapability("version", browser_version);
                chrome_options.setCapability("platform", Platform.LINUX);
                driver = new RemoteWebDriver(new URL(grid_url),chrome_options);
                break;
            case FIREFOX:
                FirefoxOptions ff_options = new FirefoxOptions();
                ff_options.setCapability("version", browser_version);
                ff_options.setCapability("platform", Platform.LINUX);
                driver = new RemoteWebDriver(new URL(grid_url),ff_options);
                break;
        }
        driver.manage().window().maximize();
    }

    @AfterClass
    @After
    public void afterClass() {
        Util.log("afterClass");
        Util.log("destroying driver.");
        if(driver != null) {
            driver.close();
            try{
                driver.quit();
            } catch (Exception ex) {
                Util.log(ex.getMessage());
            }
        }
    }

    private String checkGrid() {
        String grid_ip = ResourceBundle.getBundle("config").getString("grid_ip");
        int grid_port = Integer.parseInt(ResourceBundle.getBundle("config").getString("grid_port"));
        int ping_timeout = Integer.parseInt(ResourceBundle.getBundle("config").getString("ping_timeout"));
        String grid_url = "http://" + grid_ip + ":" + grid_port + "/wd/hub";
        Boolean can_ping = Util.pingHost(grid_ip,grid_port,ping_timeout);
        Assert.assertTrue(can_ping, "CANNOT CONNECT TO GRID AT " + grid_url);
        return grid_url;
    }

    private void scrollElementIntoMiddle(WebElement element) {
        String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
        ((JavascriptExecutor) driver).executeScript(scrollElementIntoMiddle, element);
    }

    public void click(WebElement element) {
        scrollElementIntoMiddle(element);
        element.click();
    }

    public void sendKeys(WebElement element, String text) {
        scrollElementIntoMiddle(element);
        element.sendKeys(text);
    }

    public void selectByValue(WebElement element, String text) {
        scrollElementIntoMiddle(element);
        new Select(element).selectByValue(text);
    }

    public static WebDriver getDriver()
    {
        return driver;
    }

}
